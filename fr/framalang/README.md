# Participer à Framalang



<h2>Présentation</h2>

Le groupe Framalang traduit collaborativement des nouvelles du monde du Libre. La plupart du temps, ces traductions sont destinées à être publiées sur le <a href="https://framablog.org">Framablog</a>, mais elles peuvent aussi servir à des projets amis (LQDN, April, etc.) ou viser à traduire un livre pour une édition Framabook.

Dans la joie et la bonne humeur, traductrices et traducteurs se réunissent sur une liste de discussion (voir plus bas pour l’inscription), exercent une veille du « Libre » anglophone, évaluent les travaux potentiels à traduire, puis s’attellent à la tâche !

<h2>Comment participer ?</h2>
Il faut tout d'abord s'inscrire sur la liste de diffusion. Chacun-e peut y répondre et y participer (sans spammer le reste du groupe, bien entendu !)

Ensuite, vous avez les emails et le dossier MyPads de suivi des traductions pour retrouver les travaux en cours et leur état d'avancement... donc voir ce que vous pouvez proposer !


<h3>Inscription au groupe Framalang</h3>
Nous traduisons principalement des articles originellement en <strong>Anglais</strong>.

Pour s’inscrire, suivre ce lien : <strong><a href="http://framalistes.org/sympa/subscribe/framalang">http://framalistes.org/sympa/subscribe/framalang</a></strong>

Et parce ce qu’il n’y a pas que l’anglais dans la vie, Framalang a créé une autre liste de traducteurs pour les langues <strong>espagnol</strong> (mais aussi <strong>portugais</strong>). Il va sans dire que vous y êtes également les bienvenus.

Pour s’inscrire, suivre ce lien : <strong><a href="http://framalistes.org/sympa/subscribe/framespagnol">http://framalistes.org/sympa/subscribe/framespagnol</a></strong>

Nous vous remercions par avance de vos éventuelles participations !

<h3>MyPads pour suivre les traductions</h3>
Nous essayons de regrouper tous les pads de traduction dans [un dossier MyPads partagé](https://mypads.framapad.org/mypads/?/mypads/group/framalang-7l3ibkl0/view).

Pour y avoir accès, il faut donc :
* Vous créer un compte [MyPads](https://mypads.framapad.org) (si ce n'est pas déjà fait)
* Faire un email (à la liste ou à un·e membre identifié·e du groupe) précisant votre identifiant et/ou votre email d'inscription à MyPads
* Dès qu'on vous aura rajouté·e au dossier, celui-ci apparaîtra directement dans votre compte MyPads

<h3>Sommaire de ce guide : </h3>
  * [Bienvenue chez Framalang](bienvenue.md)
  * [Le chemin d'une traduction](chemin.md) 
  * [Les outils de traduction](outils.md)
  * [Passer de la traduction à l'article Framablog](blog.md)

